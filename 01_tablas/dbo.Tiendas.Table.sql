USE [test]
GO
/****** Object:  Table [dbo].[Tiendas]    Script Date: 20/08/2018 4:24:21 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tiendas](
	[IdTienda] [int] IDENTITY(1,1) NOT NULL,
	[NombreTienda] [varchar](50) NULL,
	[IdCiudad] [int] NULL,
 CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED 
(
	[IdTienda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Tiendas] ON 

INSERT [dbo].[Tiendas] ([IdTienda], [NombreTienda], [IdCiudad]) VALUES (1, N'Barranquilla Principal', 1)
INSERT [dbo].[Tiendas] ([IdTienda], [NombreTienda], [IdCiudad]) VALUES (2, N'Barranquilla Sucursal', 1)
INSERT [dbo].[Tiendas] ([IdTienda], [NombreTienda], [IdCiudad]) VALUES (3, N'Bogota Principal', 2)
INSERT [dbo].[Tiendas] ([IdTienda], [NombreTienda], [IdCiudad]) VALUES (4, N'Bogota Sucursal', 2)
INSERT [dbo].[Tiendas] ([IdTienda], [NombreTienda], [IdCiudad]) VALUES (5, N'Cali Sucursal', 3)
INSERT [dbo].[Tiendas] ([IdTienda], [NombreTienda], [IdCiudad]) VALUES (6, N'Cartagena Sucursal', 4)
INSERT [dbo].[Tiendas] ([IdTienda], [NombreTienda], [IdCiudad]) VALUES (7, N'Medellin Sucursal', 5)
SET IDENTITY_INSERT [dbo].[Tiendas] OFF
