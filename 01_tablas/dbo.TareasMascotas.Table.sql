USE [test]
GO
/****** Object:  Table [dbo].[TareasMascotas]    Script Date: 20/08/2018 4:24:21 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TareasMascotas](
	[IdTareaMascota] [int] IDENTITY(1,1) NOT NULL,
	[IdMascota] [int] NOT NULL,
	[IdTarea] [int] NOT NULL,
	[Fecha] [datetime] NULL,
	[Hora] [datetime] NULL,
	[Estado] [int] NULL,
 CONSTRAINT [PK_TareasMascotas] PRIMARY KEY CLUSTERED 
(
	[IdTareaMascota] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TareasMascotas] ON 

INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (1, 11, 1, CAST(N'2018-08-20T10:31:17.987' AS DateTime), CAST(N'2018-08-20T08:00:00.000' AS DateTime), NULL)
INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (2, 13, 5, CAST(N'2018-08-20T10:31:17.987' AS DateTime), CAST(N'2018-08-20T12:00:00.000' AS DateTime), NULL)
INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (3, 13, 4, CAST(N'2018-08-20T10:31:17.987' AS DateTime), CAST(N'2018-08-20T11:30:00.000' AS DateTime), NULL)
INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (4, 15, 4, CAST(N'2018-08-20T10:31:17.987' AS DateTime), CAST(N'2018-08-20T11:00:00.000' AS DateTime), NULL)
INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (5, 15, 3, CAST(N'2018-08-20T00:00:00.000' AS DateTime), CAST(N'2018-08-20T14:30:00.000' AS DateTime), NULL)
INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (6, 11, 2, CAST(N'2018-08-20T10:31:17.987' AS DateTime), CAST(N'2018-08-20T13:30:00.000' AS DateTime), NULL)
INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (7, 11, 3, CAST(N'2018-08-20T10:31:17.987' AS DateTime), CAST(N'2018-08-20T13:00:00.000' AS DateTime), NULL)
INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (9, 16, 1, CAST(N'2018-08-21T00:00:00.000' AS DateTime), CAST(N'2018-08-21T01:00:00.000' AS DateTime), NULL)
INSERT [dbo].[TareasMascotas] ([IdTareaMascota], [IdMascota], [IdTarea], [Fecha], [Hora], [Estado]) VALUES (10, 16, 5, CAST(N'2018-08-20T00:00:00.000' AS DateTime), CAST(N'2018-08-20T15:00:00.000' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[TareasMascotas] OFF
