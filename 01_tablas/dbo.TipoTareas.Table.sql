USE [test]
GO
/****** Object:  Table [dbo].[TipoTareas]    Script Date: 20/08/2018 4:24:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoTareas](
	[IdTipo] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipo] [varchar](50) NULL,
 CONSTRAINT [PK_TipoTareas] PRIMARY KEY CLUSTERED 
(
	[IdTipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TipoTareas] ON 

INSERT [dbo].[TipoTareas] ([IdTipo], [NombreTipo]) VALUES (1, N'Campo')
INSERT [dbo].[TipoTareas] ([IdTipo], [NombreTipo]) VALUES (2, N'Casa')
SET IDENTITY_INSERT [dbo].[TipoTareas] OFF
