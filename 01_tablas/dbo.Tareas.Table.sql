USE [test]
GO
/****** Object:  Table [dbo].[Tareas]    Script Date: 20/08/2018 4:24:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tareas](
	[IdTarea] [int] IDENTITY(1,1) NOT NULL,
	[NombreTarea] [varchar](50) NULL,
	[CostoTarea] [decimal](18, 0) NULL,
	[IdTipo] [int] NULL,
 CONSTRAINT [PK_Tareas] PRIMARY KEY CLUSTERED 
(
	[IdTarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Tareas] ON 

INSERT [dbo].[Tareas] ([IdTarea], [NombreTarea], [CostoTarea], [IdTipo]) VALUES (1, N'Salida al parque', CAST(1000 AS Decimal(18, 0)), 1)
INSERT [dbo].[Tareas] ([IdTarea], [NombreTarea], [CostoTarea], [IdTipo]) VALUES (2, N'Entrenamiento', CAST(5000 AS Decimal(18, 0)), 1)
INSERT [dbo].[Tareas] ([IdTarea], [NombreTarea], [CostoTarea], [IdTipo]) VALUES (3, N'Vacunas', CAST(10000 AS Decimal(18, 0)), 1)
INSERT [dbo].[Tareas] ([IdTarea], [NombreTarea], [CostoTarea], [IdTipo]) VALUES (4, N'Inspección general', CAST(0 AS Decimal(18, 0)), 2)
INSERT [dbo].[Tareas] ([IdTarea], [NombreTarea], [CostoTarea], [IdTipo]) VALUES (5, N'Alimentación', CAST(12500 AS Decimal(18, 0)), 2)
SET IDENTITY_INSERT [dbo].[Tareas] OFF
