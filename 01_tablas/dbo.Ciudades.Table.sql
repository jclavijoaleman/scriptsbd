USE [test]
GO
/****** Object:  Table [dbo].[Ciudades]    Script Date: 20/08/2018 4:24:16 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ciudades](
	[IdCiudad] [int] IDENTITY(1,1) NOT NULL,
	[NombreCiudad] [varchar](50) NULL,
 CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED 
(
	[IdCiudad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Ciudades] ON 

INSERT [dbo].[Ciudades] ([IdCiudad], [NombreCiudad]) VALUES (1, N'Barranquilla')
INSERT [dbo].[Ciudades] ([IdCiudad], [NombreCiudad]) VALUES (2, N'Bogota')
INSERT [dbo].[Ciudades] ([IdCiudad], [NombreCiudad]) VALUES (3, N'Cali')
INSERT [dbo].[Ciudades] ([IdCiudad], [NombreCiudad]) VALUES (4, N'Cartagena')
INSERT [dbo].[Ciudades] ([IdCiudad], [NombreCiudad]) VALUES (5, N'Medellin')
SET IDENTITY_INSERT [dbo].[Ciudades] OFF
