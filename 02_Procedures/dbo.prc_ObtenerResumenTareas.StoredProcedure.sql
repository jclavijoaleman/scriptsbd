USE [test]
GO
/****** Object:  StoredProcedure [dbo].[prc_ObtenerResumenTareas]    Script Date: 20/08/2018 4:24:24 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[prc_ObtenerResumenTareas]
	@nombreMascota varchar(50),
	@numeroTareas int = 0,
	@idcliente int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @nombre varchar(50)
	declare @numero int

	if (@nombreMascota = '')
	begin
		set @nombre = null
	END
	else
	BEGIN
		set @nombre = @nombreMascota
	end

	if (@numeroTareas = 0)
	begin
		set @numero = null
	END
	else
	BEGIN
		set @numero = @numeroTareas
	end

    -- Insert statements for procedure here
	select m.IdMascota, m.NombreMascota, m.Apodo, m.Raza, sum(t.CostoTarea) costo, 
	(select top 1 hora from tareasmascotas where hora > getdate() and idmascota = m.IdMascota order by hora asc) FechaProxima,
	(select count(*) from tareasmascotas where hora > getdate() and idmascota = m.IdMascota) Pendientes,
	count(*) NroTareas,	
	(100 - Convert(FLOAT, (select count(*) from tareasmascotas where Convert(varchar(10), Fecha, 103) = Convert(varchar(10), GETDATE(), 103) and hora > getdate() and idmascota = m.IdMascota)) /
	Convert(FLOAT, count(*) ) * 100) Porcentaje
	from Mascotas m
	inner join TareasMascotas tm on tm.IdMascota = m.IdMascota
	inner join Tareas t on t.IdTarea = tm.IdTarea
	where Convert(varchar(10), tm.Fecha, 103) = Convert(varchar(10), GETDATE(), 103)
	 and m.IdCliente = @idcliente 
	 and m.NombreMascota = ISNULL(@nombre, m.NombreMascota)
	 and (select count(*) from tareasmascotas where idmascota = m.IdMascota) = ISNULL(@numero, (select count(*) from tareasmascotas where idmascota = m.IdMascota))
	GROUP by m.IdMascota, m.NombreMascota, m.Apodo, m.Raza

END
GO
