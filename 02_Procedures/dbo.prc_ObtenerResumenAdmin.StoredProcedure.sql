USE [test]
GO
/****** Object:  StoredProcedure [dbo].[prc_ObtenerResumenAdmin]    Script Date: 20/08/2018 4:24:24 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[prc_ObtenerResumenAdmin]
	@idTienda varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @numero int

	if (@idTienda = '')
	begin
		set @numero = null
	END
	else
	BEGIN
		set @numero = @idTienda
	end

    -- Insert statements for procedure here
	select Cedula, u.Nombre + ' ' + u.Apellido Nombre , t.NombreTienda, m.Apodo, count(*) NumTareas  from Usuarios u
	inner join Tiendas t on t.IdTienda = u.IdTienda
	inner join Mascotas m on m.IdCliente = u.Id
	left join TareasMascotas tm on tm.IdMascota = m.IdMascota
	where Convert(varchar(10), tm.Fecha, 103) = Convert(varchar(10), GETDATE(), 103)
	 and t.IdTienda = ISNULL(@numero, t.IdTienda)
	GROUP by Cedula, u.Nombre, u.Apellido, t.NombreTienda, m.Apodo

END
GO
